import { Box, Heading, Text } from "@chakra-ui/react";

import Head from "next/head";

import Header from "@components/header";
import Footer from "@components/footer";

import { Component } from "react";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minH: 600,
    };

    this.manageState = (() => this.setState({ minH: window.innerHeight })).bind(
      this
    );
  }

  render() {
    return (
      <Box d="flex" flexDir="column" minH={this.state.minH}>
        <Head>
          <title>Aabi</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Header />
        <Box
          flex="1 1 auto"
          bgGradient="linear(to-t, gray.900, black)"
          py={{ base: 8, sm: 16, md: 24 }}
          w="100%"
          color="white"
          px={{ base: 4 }}
        >
          <Heading
            id="about-this-website"
            fontFamily="'Roboto', sans-serif"
            textAlign="center"
            my={{ base: 8, md: 12 }}
            fontSize={{ base: "2xl", md: "3xl", lg: "4xl" }}
          >
            About this website
          </Heading>
          <Text
            fontSize={{ base: "lg", lg: "xl" }}
            className="content"
            as="div"
            textAlign="justify"
          >
            <p>
              This website is my version control, a program to keep track of my
              progress. I will write about my opinions on technology, my thought
              process/ workthrough of a project, maybe some reviews and anything
              that I want to write. I am not good at UI/UX so it won’t be the
              greatest website, but it will be simple, responsive and functional
              and that’s all that matters.
              <br />
              About the techstack of the website, it is made with{" "}
              <a href="https://nextjs.org/">NEXT.js</a> for Server Side
              Rendering, <a href="https://chakra-ui.com">Chakra UI</a> for the
              components library.
              <br />I used NEXT.js as I am learning this framework. I also
              considered <a href="https://gatsbyjs.com">Gatsby</a> for the
              purpose, as I don’t know much about GraphQL at the moment and also
              needed to gave a content management system (More on this later).
              Now that the template for the website is done, I guess the choice
              doesn’t matter anyway.
              <br />I knew I didn’t want to bother with creating the website
              from scratch, even with <a href="https://reactjs.org">React</a>.
              So, I looked at{" "}
              <a href="https://tailwindcomponents.com">Tailwind</a>,{" "}
              <a href="https://material-ui.com">Material UI</a>, and Chakra UI.
              There wasn’t very much difference between these for me, so I used
              Chakra UI as I had heard about it from{" "}
              <a href="https://www.youtube.com/channel/UC-8QAzbLcRglXeN_MY9blyw">
                Ben Awad
              </a>
              .<br />
              Now, for the database, the site doesn’t currently use any. I
              considered using <a href="https://cosmicjs.com">CosmicJS</a>,
              MySQL database, but I decided to not use these. Currently, I
              manually convert markdown to html with pandoc and add it to a page
              in the site. I am open to suggestions for better methods on this
              topic.
            </p>
          </Text>
        </Box>
        <Footer />
      </Box>
    );
  }

  componentDidMount() {
    window.addEventListener("resize", this.manageState);
    this.manageState();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.manageState);
  }
}
