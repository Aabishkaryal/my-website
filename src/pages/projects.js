import { Box, Heading, Text } from "@chakra-ui/react";

import Head from "next/head";

import Header from "@components/header";
import Footer from "@components/footer";
import Projects from '@components/projects';

import { Component } from "react";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minH: 600,
    };

    this.manageState = (() => this.setState({ minH: window.innerHeight })).bind(
      this
    );
  }

  render() {
    return (
      <Box d="flex" flexDir="column" minH={this.state.minH}>
        <Head>
          <title>Aabi</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Header />
        <Projects />
        <Footer />
      </Box>
    );
  }

  componentDidMount() {
    window.addEventListener("resize", this.manageState);
    this.manageState();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.manageState);
  }
}
