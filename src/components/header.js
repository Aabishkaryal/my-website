import { Box, Text } from "@chakra-ui/react";
import { Navigation } from "./navigation";

export default function Header() {
  const { title } = getProps();
  return (
    <Box
      d="flex"
      justifyContent="space-between"
      px={["30px"]}
      py={["10px"]}
      position="sticky"
      top="0"
      boxShadow="md"
      flex="0 1 auto"
      alignItems="center"
      zIndex="sticky"
      bgColor="blue.900"
    >
      <Text
        fontFamily="'Roboto', sans-serif"
        fontSize={{ base: "2xl", md: "3xl" }}
        fontWeight="500"
        as="header"
        color="white"
        sx={{ userSelect: "none" }}
      >
        {title}
      </Text>
      <Navigation />
    </Box>
  );
}

function getProps() {
  return {
    title: "Aabi",
  };
}
