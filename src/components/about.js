import { Box, Heading, Text } from "@chakra-ui/react";

import Link from "next/link";

export default function About() {
  return (
    <Box
      px={{ base: 4, sm: 6, md: 10, lg: 12, xl: 16 }}
      py={{ base: 4, sm: 6, md: 10 }}
      bgColor="gray.900"
    >
      <Heading
        fontFamily="'Roboto', sans-serif"
        textAlign="center"
        my={{ base: 8, md: 12 }}
        fontSize={{ base: "2xl", md: "3xl", lg: "4xl" }}
        fontWeight="normal"
        color="whiteAlpha.900"
      >
        About this website
      </Heading>
      <Text
        fontSize={{ base: "md", lg: "xl" }}
        className="content"
        as="div"
        textAlign="justify"
        color="whiteAlpha.700"
        fontFamily="'News reader', serif"
      >
        This website is home for all of my{" "}
        <Link href="/projects">projects</Link> (all kinds). The website itself
        is a work in progress. So, any kind of help is appreciated since I'm not
        that great at UI/UX designing. If I sound like a guy, you would like to
        work with or talk with, feel free to send me an {" "}
        <Link href="mailto:aabishkaryal@gmail.com">email</Link>.
      </Text>
    </Box>
  );
}
