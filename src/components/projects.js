import { Box, Heading, Text } from "@chakra-ui/react";
import Link from "next/link";

export default function Products() {
  return (
    <Box
      px={{ base: 4, sm: 6, md: 10, lg: 12, xl: 16 }}
      py={{ base: 4, sm: 6, md: 10 }}
      bgColor="gray.900"
      flex="1 1 auto"
    >
      <Heading
        fontFamily="'Roboto', sans-serif"
        textAlign="center"
        my={{ base: 8, md: 12 }}
        fontSize={{ base: "2xl", md: "3xl", lg: "4xl" }}
        fontWeight="normal"
        color="whiteAlpha.900"
      >
        My Projects
      </Heading>
      <Box>
        <Text
          fontSize={{ base: "xl", lg: "2xl" }}
          className="content"
          as="div"
          textAlign="justify"
          color="whiteAlpha.700"
          fontFamily="'News reader', serif"
          textAlign="center"
        >
            Work in Progress.
        </Text>
      </Box>
    </Box>
  );
}
