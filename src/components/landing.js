import { Box, Text } from "@chakra-ui/react";

export default function Landing() {
  return (
    <Box
      flex="1 1 auto"
      py={{ base: 8, sm: 16, md: 24 }}
      bgColor="gray.900"
      w="100%"
      textAlign="center"
      color="white"
      px={{ base: 4 }}
    >
      <Text
        as="h1"
        fontFamily="'Roboto', sans-serif"
        fontSize={{ base: "2xl", md: "3xl", lg: "4xl" }}
        py={{ base: 6 }}
      >
        Hey! I'm Aabishkar.
      </Text>
      <Text
        py={{ base: 6 }}
        fontFamily="'News Reader', serif"
        fontSize={{ base: "lg", lg: "xl" }}
        color="gray.400"
      >
        I'm a software developer who enjoys playing chess and watching TV Shows.
      </Text>
    </Box>
  );
}
