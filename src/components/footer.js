import { FaGithub, FaGitlab, FaLinkedinIn } from "react-icons/fa";
import { HiMail } from "react-icons/hi";
import { Icon } from "@chakra-ui/icons";
import { Box, Link, Tooltip } from "@chakra-ui/react";

export default function Footer() {
  const { gitlab, github, email, linkedIn } = getProps();
  const margin = { base: 3, sm: 4, md: 5, lg: 6 };
  const boxSize = { base: "1.5em", lg: "2em" };
  const color = "white";
  return (
    <Box
      d="flex"
      justifyContent="center"
      bgColor="blackAlpha.900"
      flex="0 1 auto"
      p={{ base: 1 }}
    >
      <Social
        href={gitlab}
        m={margin}
        as={FaGitlab}
        boxSize={boxSize}
        color={color}
        label={gitlab}
        ariaLabel={`Link to:${gitlab}`}
        name="GitLab"
      />
      <Social
        href={github}
        m={margin}
        as={FaGithub}
        boxSize={boxSize}
        color={color}
        label={github}
        ariaLabel={`Link to:${github}`}
        name="GitHub"
      />
      <Social
        href={linkedIn}
        m={margin}
        as={FaLinkedinIn}
        boxSize={boxSize}
        color={color}
        label={linkedIn}
        ariaLabel={`Link to:${linkedIn}`}
        name="LinkedIn"
      />
      <Social
        href={`mailto:${email}`}
        m={margin}
        as={HiMail}
        boxSize={boxSize}
        color={color}
        label={email}
        ariaLabel={`Mail to: ${email}`}
        name="Email"
      />
    </Box>
  );
}

function Social({ href, m, as, boxSize, color, label, ariaLabel, name }) {
  return (
    <Tooltip label={label} aria-label={ariaLabel}>
      <Link href={href} m={m} target="_blank" name={name} rel="noopener">
        <Icon as={as} boxSize={boxSize} color={color} />
      </Link>
    </Tooltip>
  );
}

function getProps() {
  return {
    email: "aabishkaryal@gmail.com",
    github: "https://github.com/aabishkaryal",
    gitlab: "https://gitlab.com/aabishkaryal",
    linkedIn: "https://linkedin.com/in/aabishkaryal",
  };
}
