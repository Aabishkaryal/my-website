import {
  Box,
  IconButton,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerBody,
  useMediaQuery,
  useDisclosure,
  VStack,
  StackDivider,
  Text,
  HStack,
} from "@chakra-ui/react";

import { HamburgerIcon } from "@chakra-ui/icons";
import { useRef } from "react";

import Link from "next/link";

export function Navigation() {
  const { links } = getProps();
  const [isLargerThan768] = useMediaQuery(["(min-width:48em)"]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const hamBurgerRef = useRef();
  const drawerCloseButtonRef = useRef();
  if (isLargerThan768) {
    return (
      <HStack>
        {links.map(({ name, href }, i) => (
          <Link href={href} key={i}>
            <Text
              fontFamily="'NewsReader', serif"
              fontSize={{ md: "md", xl: "lg" }}
              mx={{ md: 3, xl: 6 }}
              as="a"
              cursor="pointer"
              color="gray.400"
              fontWeight="normal"
              _hover={{
                color: "white",
              }}
            >
              {name}
            </Text>
          </Link>
        ))}
      </HStack>
    );
  } else {
    return (
      <Box>
        <IconButton
          aria-label="navigation icon"
          icon={
            <HamburgerIcon
              boxSize={{ base: "1.25em", sm: "1.5em" }}
              color="gray.400"
              _hover={{
                color: "white",
              }}
            />
          }
          p={{ base: 2, sm: 3 }}
          ref={hamBurgerRef}
          onClick={onOpen}
          bgColor="black"
          _hover={{
            bgColor: "black",
          }}
          _active={{
            bgColor: "black",
          }}
        />
        <Drawer
          isOpen={isOpen}
          placement="left"
          onClose={onClose}
          finalFocusRef={hamBurgerRef}
          size="xs"
          initialFocusRef={drawerCloseButtonRef}
        >
          <DrawerOverlay>
            <DrawerContent bgColor="black">
              <DrawerCloseButton
                ref={drawerCloseButtonRef}
                size="1.25em"
                m="8px"
                color="white"
              />
              <DrawerBody mt="50px">
                <VStack
                  divider={<StackDivider borderColor="gray.300" />}
                  spacing={4}
                >
                  {links.map(({ name, href }, i) => (
                    <Link href={href} key={i}>
                      <Text
                        fontFamily="'NewsReader', serif"
                        fontSize="1.5rem"
                        as="a"
                        cursor="pointer"
                        onClick={onClose}
                        fontWeight="normal"
                        color="gray.400"
                        _hover={{
                          color: "white",
                        }}
                      >
                        {name}
                      </Text>
                    </Link>
                  ))}
                </VStack>
              </DrawerBody>
            </DrawerContent>
          </DrawerOverlay>
        </Drawer>
      </Box>
    );
  }
}

function getProps() {
  return {
    links: [
      { name: "Home", href: "/" },
      { name: "Projects", href: "/projects" },
    ],
  };
}
